const startupDebug = require('debug')('app:startup');
const dbDebug = require('debug')('app:dataBase');
const config = require('config');
const helmet = require('helmet');
const morgan = require('morgan');
const Joi = require('joi');
const express = require('express');
const logger = require('./middleware/logger');
const courses = require('./routes/courses');
const home = require('./routes/home');

const app = express();

app.use(express.json());
app.use(helmet());
app.use('/api/courses', courses);
app.use('/', home);
//custom middleware
app.use(logger);

//use pug
app.set('view engine', 'pug');
app.set('views', './views');

//configuration
startupDebug('Application Name', config.get('name'));
startupDebug('Application Host', config.get('mail.host'));
startupDebug('Application password', config.get('mail.password'));

if (app.get('env') === 'developement') {
  app.use(morgan('tiny'));
  startupDebug('morgan is running...');
}

//db log...
dbDebug('Data Base is ready...');

const port = process.env.PORT || 3000;

app.listen(port, () => {console.log(`Listening on port ${port}`)});
