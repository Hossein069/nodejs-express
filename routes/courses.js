const express = require('express');
const router= express.Router();

let courses = [
    { id: 1, name: 'course 1'},
    { id: 2, name: 'course 2'},
    { id: 3, name: 'course 3'},
  ];

router.get('/', (req, res) => {
    res.send(courses);
  });
  
  router.get('/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) {
      res.status(404).send('The course with the given ID dose not exist');
    } else {
      res.send(course);
    }
  });
  
  //Add a course
  router.post('/', (req, res) => {
  
    //Do validation
    const { error } = validate(req.body);
    if (error) {
      res.status(400).send(error.details[0].message);
      return;
    }
  
    const course = {
      id: courses.length + 1,
      name: req.body.name
    };
    courses.push(course);
    res.send(course);
  });
  
  //Update a course
  router.put('/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) {
      res.status(404).send('There is not such course');
    }
    else {
      //Do validation
      const { error } = validate(req.body);
      if (error) {
        res.status(400).send(error.details[0].message);
        return;
      }
  
      course.name = req.body.name;
      res.send(courses);
  
    }
  });
  
  router.delete('/:id', (req, res) => {
    const courseIndex = courses.findIndex(c => c.id === parseInt(req.params.id));
    if (!courses[courseIndex]) {
      res.status(404).send('There is not such course');
    }
    courses.splice(courseIndex, 1);
    res.send(courses);
  });
  
  function validate(course) {
  
    const schema = Joi.object({
      name: Joi.string().min(3).required()
    });
  
    return schema.validate(course);
  }

  module.exports = router;